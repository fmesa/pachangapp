package cat.dam.mesa.pachangapp_20;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class Chat extends Activity {

    //**COMU
    private ImageView iv_esport, iv_mapa,iv_chat,iv_conf;
    //**


    //Firebase
    private DatabaseReference bd;
    private String name = "";
    private FirebaseUser user;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //**NOM USUARI
        user = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseFirestore.getInstance();

        db.collection("usuaris").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if(documentSnapshot.exists() && documentSnapshot != null){
                        String s = documentSnapshot.getString("nom");
                        name = s;
                    }
                }
            }
        });
        //**

        //**COMU
        iv_esport = findViewById(R.id.iv_esport);
        iv_mapa = findViewById(R.id.iv_mapa);
        iv_chat = findViewById(R.id.iv_chat);

        iv_esport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Esport.class));
            }
        });
        iv_mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Mapa.class));
            }
        });
        //**

        //MISSATGES

        bd = FirebaseDatabase.getInstance().getReference("Missatge");
        final TextView tv_chat = (TextView) findViewById(R.id.tv_chat);



        bd.addValueEventListener(new ValueEventListener() {



            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String [] missatges = dataSnapshot.getValue().toString().replace("{", " ").replace("}","").split(",");
                ArrayList<String> missatgesOrdenats = new ArrayList<String>(Arrays.asList(missatges));

                Collections.sort(missatgesOrdenats);

                tv_chat.setText(""); //Netejar l'area de text

                for(int i = 0; i<missatgesOrdenats.size() ;i++){
                    String[] finalM = missatgesOrdenats.get(i).split("=");
                    tv_chat.append(finalM[1]+ "\n");



                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                tv_chat.setText("Cancel·lat");
            }
        });
    }

    //ENVIAR NOM USUARI + MISSATGE
    public void enviarMissatge(View v){
        final EditText et_chat = (EditText) findViewById(R.id.et_chat);

        bd.child(Long.toString(System.currentTimeMillis())).setValue(name+" : "+et_chat.getText().toString());
        et_chat.setText("");

    }
}
