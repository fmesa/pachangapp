package cat.dam.mesa.pachangapp_20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class sexe extends Activity {
    private Button btn_desar;
    private TextView tv_cancelar;
    private RadioGroup  radioGrp;
    private RadioButton radioSexButton;

    private FirebaseFirestore db;
    private final String COLLECTION_KEY="usuaris";
    private FirebaseUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sexe);

        btn_desar = (Button) findViewById(R.id.btn_desar);
        tv_cancelar = (TextView) findViewById(R.id.tv_cancelar);
        radioGrp  = (RadioGroup) findViewById(R.id.radioGrp);
        db = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();


        tv_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        db.collection("usuaris").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if(documentSnapshot.exists() && documentSnapshot != null){
                        String s = documentSnapshot.getString("sexe");
                        if(s.equals("Home")){
                            radioGrp.check(R.id.radioM);
                        }else{
                            radioGrp.check(R.id.radioF);
                        }
                    }
                }
            }
        });

        btn_desar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                btn_desar.setEnabled(false);

                int selectedId = radioGrp.getCheckedRadioButtonId();

                radioSexButton = (RadioButton) findViewById(selectedId);

                Toast.makeText(sexe.this,
                        radioSexButton.getText(), Toast.LENGTH_SHORT).show();
                db.collection(COLLECTION_KEY).document(user.getUid()).update("sexe",radioSexButton.getText());
                finish();
            }

        });
    }
}
