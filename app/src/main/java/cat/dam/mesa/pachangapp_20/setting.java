package cat.dam.mesa.pachangapp_20;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class setting extends Activity {

    private Usuari usuari;
    private ArrayList<Usuari> llistaUsuaris = new ArrayList<Usuari>();
    private String missatge="";
    //etiqueta per fer debug amb el Logcat i base de dades
    private final String TAGLOG ="FirebaseFireStore";
    FirebaseFirestore db;
    FirebaseUser user;
    private String uid;
    private final String COLLECTION_KEY="usuaris";
    private Query query;
    private FirebaseAuth mAuth;

    private ImageView mHBack;
    private ImageView mHHead;
    private ImageView mUserLine;
    private TextView mUserName;
    private TextView mPhone;

    private Button surt;
    private ImageView back;
    private ItemView nom;
    private ItemView sexe;
    private ItemView idioma;
    private ItemView suggeriment;
    private ItemView versio;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initView();
        setData();


    }

    @Override
    protected void onResume() {
        super.onResume();
        db.collection("usuaris").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if(documentSnapshot.exists() && documentSnapshot != null){
                        String sNom = documentSnapshot.getString("nom");
                        String sSexe = documentSnapshot.getString("sexe");
                        mUserName.setText(sNom);
                        nom.setRightDesc(sNom);

                        sexe.setRightDesc(sSexe);
                        if(sSexe.equals("Home")){
                            sexe.setLeftIcon(R.drawable.ic_man);
                        }else{
                            sexe.setLeftIcon(R.drawable.ic_dona);
                        }

                    }
                }
            }
        });
    }

    /**********************************************************************************/


    private void setData() {
        /**********************************************************************************/
        /****Codi Tancat****/
        //efect background;
        Glide.with(this).load(user.getPhotoUrl())
                .bitmapTransform(new BlurTransformation(this, 25), new CenterCrop(this))
                .into(mHBack);
        //set circle photo;
        Glide.with(this).load(user.getPhotoUrl())
                .bitmapTransform(new CropCircleTransformation(this))
                .into(mHHead);
        /****Codi Tancat****/
        /**********************************************************************************/

        sexe.setItemClickListener(new ItemView.itemClickListener() {
            @Override
            public void itemClick(String text) {
                //Creem l'Intent
                Intent intent = new Intent(setting.this, sexe.class);
                //Iniciem la nova activitat
                startActivity(intent);
            }
        });



        versio.setItemClickListener(new ItemView.itemClickListener() {
            @Override
            public void itemClick(String text) {
                //Creem l'Intent
                Intent intent = new Intent(setting.this, version.class);
                //Iniciem la nova activitat
                startActivity(intent);
            }
        });

        suggeriment.setItemClickListener(new ItemView.itemClickListener() {
            @Override
            public void itemClick(String text) {
                //Creem l'Intent
                Intent intent = new Intent(setting.this, suggeriment.class);
                setting.this.startActivity(intent);
                // setting.this.finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //comprovem si ja existeix id, add user to firestore.
        db.collection(COLLECTION_KEY)
                .whereEqualTo("uid", user.getUid())
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    QuerySnapshot querySnapShot = task.getResult();
                    if (querySnapShot.size() > 0) {

                        db.collection("usuaris").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot documentSnapshot = task.getResult();
                                    if(documentSnapshot.exists() && documentSnapshot != null){
                                        String s = documentSnapshot.getString("nom");
                                        String s2 = documentSnapshot.getString("sexe");
                                        mUserName.setText(s);
                                        nom.setRightDesc(s);
                                        sexe.setRightDesc(s2);
                                        if(s2.equals("Home")){
                                            sexe.setLeftIcon(R.drawable.ic_man);
                                        }else{
                                            sexe.setLeftIcon(R.drawable.ic_dona);
                                        }

                                    }
                                }
                            }
                        });
                    } else {
                        usuari = new Usuari(user.getUid(),user.getDisplayName());
                        usuari.setSexe("");
                        db.collection(COLLECTION_KEY).document(user.getUid()).set(usuari).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void thisVoid) {

                            }
                        });
                        //set name
                        String uid = user.getUid();
                        String name = user.getDisplayName();
                        mUserName.setText(name);
                        nom.setRightDesc(name);
                    }
                } else {
                    // missatge = "ERROR: No s'ha pogut comprovar l'usuari (" + task.getException() + ")";
                }
            }
        });

        //set numero del telefon;
        if((user.getPhoneNumber()!= null)){
            mPhone.setText(user.getPhoneNumber());
        }
        // mUserphone.setText(user.getPhoneNumber());

        nom.setItemClickListener(new ItemView.itemClickListener() {
            @Override
            public void itemClick(String text) {
                //Creem l'Intent
                Intent intent = new Intent(setting.this, canviarNom.class);
                //Iniciem la nova activitat
                startActivity(intent);

            }
        });


        surt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creem l'Intent
                surt.setEnabled(false);
                //AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
                //alertDialog.setTitle(getString(R.string.add_dialog_title));
                //alertDialog.setMessage(getString(R.string.OutAdvise));
                // alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                //        new DialogInterface.OnClickListener() {
                //            public void onClick(DialogInterface dialog, int which) {
                //                 surt.setEnabled(true);
                //                dialog.dismiss();
                //           }
                //       });
                //  alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.btnPositive),
                //        new DialogInterface.OnClickListener() {
                //            @Override
                //           public void onClick(DialogInterface dialog, int which) {

                                mAuth.signOut();
                                Intent intent = new Intent(setting.this, MainActivity.class);
                                FirebaseAuth.getInstance().signOut();
                                //Iniciem la nova activitat
                                startActivity(intent);
                                finish();
                            }
                 });
        //alertDialog.show();
    }


    private void initView() {

        user = FirebaseAuth.getInstance().getCurrentUser();

        db = FirebaseFirestore.getInstance();
        uid = user.getUid();
        //control part de superior.
        mHBack = (ImageView) findViewById(R.id.h_back);
        mHHead = (ImageView) findViewById(R.id.h_head);
        mUserLine = (ImageView) findViewById(R.id.user_line);
        mUserName = (TextView) findViewById(R.id.user_name);
        mPhone = (TextView) findViewById(R.id.user_phone);
        //control item

        back = (ImageView) findViewById(R.id.back);
        nom = (ItemView) findViewById(R.id.nom);
        sexe = (ItemView) findViewById(R.id.sexe);
        suggeriment = (ItemView) findViewById(R.id.suggeriment);
        versio = (ItemView) findViewById(R.id.versio);

        surt = (Button) findViewById(R.id.surt);
        mAuth = FirebaseAuth.getInstance();
    }



}