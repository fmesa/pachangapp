package cat.dam.mesa.pachangapp_20;

//Views
//Control de tasques
//Accés a base de dades de Firebase (Firestore)
//Debug
//Per afegir registres a la base de dades i ordenar-los

public class Usuari {
    private String uid;
    private String nom;
    private String sexe;
    //getters i setters
    //constructors
    public Usuari() {}

    //és necessari explicitar un constructor public sense arguments
    public Usuari(String uid, String nom) {
        this.uid = uid;
        this.nom = nom;
    }
    public Usuari(String uid, String nom,String sexe) {
        this.uid = uid;
        this.nom = nom;
        this.sexe = sexe;
    }
    public String getUid() {
        return uid;
    }
    public String getNom() {
        return nom;
    }
    public String getSexe() {return sexe;}

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
    public void setUid(String uid) { this.uid = uid; }


    public String toString(){
//Important fer mètode .toString per alimentar correctament Spinner o ListView
//ja que és el mètode que es crida automàticament per representar els objectes.
        return (nom);
    }
}
