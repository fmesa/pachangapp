package cat.dam.mesa.pachangapp_20;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class canviarNom extends Activity {

    private Button btn_desar;
    private TextView tv_cancelar;
    static EditText et_nom;
    private FirebaseFirestore db;
    private final String COLLECTION_KEY="usuaris";
    private FirebaseUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canviar_nom);


        btn_desar = (Button) findViewById(R.id.btn_desar);
        tv_cancelar = (TextView) findViewById(R.id.tv_cancelar);
        et_nom = (EditText) findViewById(R.id.et_nom);

        db = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();


        tv_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        db.collection("usuaris").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if(documentSnapshot.exists() && documentSnapshot != null){
                        String s = documentSnapshot.getString("nom");
                        et_nom.setText(s);
                    }
                }
            }
        });

        et_nom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(et_nom.toString() != "") {
                    btn_desar.setEnabled(true);
                }else {
                    btn_desar.setEnabled(false);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    btn_desar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            btn_desar.setEnabled(false);
            String s = et_nom.getText().toString();
            db.collection(COLLECTION_KEY).document(user.getUid()).update("nom",s);
            finish();
        }
    });

    }
}
