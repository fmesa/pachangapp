package cat.dam.mesa.pachangapp_20;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Esport extends Activity {

    //**COMU
    private ImageView iv_esport, iv_mapa,iv_chat,iv_conf;
    private LinearLayout ll_futbol, ll_basquet, ll_tennis, ll_rugbi, ll_padel, ll_volley;
    //**

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esport);

        //**COMU
        iv_esport = findViewById(R.id.iv_esport);
        iv_mapa = findViewById(R.id.iv_mapa);
        iv_chat = findViewById(R.id.iv_chat);
        iv_conf = findViewById(R.id.iv_conf);
        //**
        ll_futbol = findViewById(R.id.ll_futbol);
        ll_basquet = findViewById(R.id.ll_basquet);
        ll_tennis = findViewById(R.id.ll_tennis);
        ll_rugbi = findViewById(R.id.ll_rugbi);
        ll_padel = findViewById(R.id.ll_padel);
        ll_volley = findViewById(R.id.ll_volley);

        //**COMU
        iv_mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Mapa.class));
            }
        });
        iv_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Chat.class));
            }
        });
        iv_conf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), setting.class));
            }
        });
        //**
        ll_futbol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.futbol);
            }
        });
        ll_basquet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.basquet);
            }
        });
        ll_tennis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.tenis);
            }
        });
        ll_rugbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.rugbi);
            }
        });
        ll_padel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.padel);
            }
        });
        ll_volley.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.volley);
            }
        });
    }

}