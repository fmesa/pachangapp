package cat.dam.mesa.pachangapp_20;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class suggeriment extends Activity {
    private ImageView back;
    private EditText suggeriment;
    private Button enviar;
    private FirebaseFirestore db;
    private final String COLLECTION_SUGGERIMENT="suggeriment";
    private UsuariSuggerer usuariSuggerer;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggeriment);

        db = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();

        back = (ImageView) findViewById(R.id.back);
        suggeriment = (EditText) findViewById(R.id.suggeriment);
        enviar = (Button) findViewById(R.id.enviar);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creem l'Intent
                //Intent intent = new Intent(suggeriment.this, setting.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                //  suggeriment.this.startActivity(intent);
                //suggeriment.this.finish();
                finish();
            }
        });

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviar.setEnabled(false);
                String s = suggeriment.getText().toString();
                usuariSuggerer = new UsuariSuggerer(user.getUid(),user.getDisplayName(),s);

                db.collection(COLLECTION_SUGGERIMENT)
                        .add(usuariSuggerer)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {

                                Toast.makeText(suggeriment.this, "S'ha enivat, gràcies per la seva proposta ",
                                        Toast.LENGTH_SHORT).show();
                                suggeriment.setText("");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(suggeriment.this, "Error! No s'ha enivat.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                enviar.setEnabled(true);
            }
        });

        suggeriment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                processButtonByTextLength();
            }
        });

        // Listen to EditText key event to change button state and text accordingly.
        suggeriment.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                // Get key action, up or down.
                int action = keyEvent.getAction();

                // Only process key up action, otherwise this listener will be triggered twice because of key down action.
                if(action == KeyEvent.ACTION_UP)
                {
                    processButtonByTextLength();
                }
                return false;
            }
        });
    }
    // Enable or disable and change button text by EditText text length.
    private void processButtonByTextLength()
    {
        String inputText = suggeriment.getText().toString();
        if(inputText.length() > 20)
        {
            enviar.setEnabled(true);
        }else
        {
            enviar.setEnabled(false);
        }
    }
    }
